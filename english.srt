1
00:00:03,045 --> 00:00:07,147
WOMAN: There's no music ?
MAN: No, well yes - a few songs, later in the film.

2
00:00:08,078 --> 00:00:10,867
WOMAN: Alalah. It's a superproduction !

3
00:00:11,289 --> 00:00:15,054
MAN: We started out saying "Year Zero-One, a film to be made together" - and as it turns out we were many.

4
00:00:15,079 --> 00:00:16,601
Not all at the same time, obviously.

5
00:00:16,695 --> 00:00:18,271
WOMAN: A studio would have been too small.

6
00:00:18,397 --> 00:00:22,468
MAN: There wasn't any studio, it was shot in the streets, in the fields... a bit everywhere.

7
00:00:22,578 --> 00:00:23,641
Those are the actors.

8
00:00:23,865 --> 00:00:25,138
WOMAN: It's unreadable...

9
00:00:25,311 --> 00:00:27,724
MAN: Doesn't matter... we'll recognize the comedians as we go.

10
00:00:27,749 --> 00:00:29,747
WOMAN: What about the others ?
MAN: They'll recognize themselves.

11
00:00:29,928 --> 00:00:31,591
WOMAN: Hey, here's Paco on the right - I know him.

12
00:00:31,926 --> 00:00:34,279
He calls himself Paco because he's wanted by the police.

13
00:00:34,304 --> 00:00:36,152
But in reality his name is Manuel [?]

14
00:00:36,177 --> 00:00:37,896
MAN: Sht. It's starting.

15
00:00:38,740 --> 00:00:39,661
- Hello, sir.

16
00:00:39,701 --> 00:00:43,629
- I've seen you... but haven't had the chance to talk to you.

17
00:00:43,802 --> 00:00:45,903
- Don't you usually get on one of the front cars...?

18
00:00:45,928 --> 00:00:52,036
- Yes, I tend to get on before the first class passengers, I find there are fewer people.

19
00:00:52,834 --> 00:00:56,115
- I always get on around here... habits, you know.

20
00:00:58,943 --> 00:01:01,254
- Been taking this train for twelve years now.

21
00:01:01,294 --> 00:01:03,677
- Hmmm... me too - more or less.

22
00:01:03,702 --> 00:01:05,513
- You didn't get on yesterday though.

23
00:01:05,623 --> 00:01:06,889
- Ah, you noticed ?

24
00:01:08,075 --> 00:01:12,355
- You're right, I, uh... missed it. I got on the next one.

25
00:01:12,380 --> 00:01:14,904
- You didn't really miss it, you just didn't get on.

26
00:01:15,991 --> 00:01:19,029
- Yes that's right... I got on the next one.

27
00:01:21,026 --> 00:01:23,500
- And you didn't get on... on purpose ?

28
00:01:24,334 --> 00:01:25,965
- Yes that's silly.

29
00:01:25,990 --> 00:01:27,594
Suddenly I...

30
00:01:28,534 --> 00:01:31,537
I felt like... not getting on the train.

31
00:01:33,838 --> 00:01:37,805
- Not getting on the train ever again ?
- No, no, never's not the word, but...

32
00:01:38,420 --> 00:01:40,928
Always the same train, you know, after a while...

33
00:01:41,354 --> 00:01:43,854
- Together sounds easier, don't you think ?

34
00:01:44,585 --> 00:01:45,608
- What do you mean ?

35
00:01:45,649 --> 00:01:46,918
- Not getting on the train.

36
00:01:47,786 --> 00:01:48,888
- I don't know.

37
00:01:57,168 --> 00:02:00,080
The good thing is, we're not afraid of, uh...

38
00:02:02,861 --> 00:02:06,016
They're not going to slap us, I mean - we've got nothing to fear.

39
00:02:06,041 --> 00:02:11,135
And them ? You think the fear of being slapped keeps them going ?

40
00:02:15,823 --> 00:02:17,377
They can't !

41
00:02:18,690 --> 00:02:21,385
They can't behave like this !

42
00:02:22,807 --> 00:02:24,817
We ought to tell them: "you're too big" !

43
00:02:24,842 --> 00:02:27,505
"Too big to be slapped, to be kicked in the butt!"

44
00:02:27,538 --> 00:02:29,824
We ought to tell them !

45
00:02:30,307 --> 00:02:33,292
Now you just said something huge, because see - it's been, what ?

46
00:02:33,317 --> 00:02:36,514
It's been ten minutes since we "escaped", hm ?

47
00:02:36,975 --> 00:02:40,335
And in truth, now we have something to tell people.

48
00:02:40,949 --> 00:02:42,494
How are we going to say it?

49
00:02:42,519 --> 00:02:44,254
I don't know, er...

50
00:02:44,664 --> 00:02:48,664
We're going to, er... find something...

51
00:02:48,935 --> 00:02:49,544
Gasoline ?

52
00:02:49,577 --> 00:02:50,718
Yes, 50 <i>francs</i>.

53
00:02:50,743 --> 00:02:53,743
- Prepare to save on gas, better be careful !

54
00:02:54,057 --> 00:02:57,534
- What does he mean? Is a strike announced?
- I don't know.

55
00:02:57,559 --> 00:02:59,934
- I haven't read anything about it.
- Me neither.

56
00:02:59,959 --> 00:03:03,014
- Say, I think I'm out of change, can you see if you've got 50 <i>francs</i>?
- Sure.

57
00:03:03,039 --> 00:03:04,438
Let me see.

58
00:03:05,425 --> 00:03:06,792
 No, I'm out.

59
00:03:07,051 --> 00:03:09,386
Hey, what did you mean by "save on gas"?

60
00:03:09,426 --> 00:03:12,243
Refinery workers are taking up music classes!

61
00:03:12,268 --> 00:03:15,851
- You have to be joking!
- No at all, it's official: soon it'll be the year zero-one.

62
00:03:15,884 --> 00:03:18,633
- And you believe this stuff?
- You bet I do!

63
00:03:18,658 --> 00:03:20,836
All week long, filling up tanks - you bet I believe this stuff!

64
00:03:20,861 --> 00:03:23,079
If anybody does it, it'll be me.

65
00:03:23,104 --> 00:03:25,874
How will you put food on the table? Hunting bisons?

66
00:03:25,899 --> 00:03:31,491
We'll send everyone who makes this crap till the soil with their feet - this way we won't be short on wheat.

67
00:03:31,522 --> 00:03:35,511
Here's a gift - for every full tank, one plastic flower.

68
00:03:35,563 --> 00:03:39,742
- Say, meanwhile you'll just watch wheat grow?
- Doesn't seem unpleasant.

69
00:03:39,767 --> 00:03:44,650
- You'll die of boredom, pal - we're on Earth to work!
- Not necessarily, not at all!

70
00:03:44,675 --> 00:03:48,255
Anyway we'll have time to think about all this and decide what we're on Earth to do, work or not.

71
00:03:48,287 --> 00:03:51,253
And if we end up having to work, we can always shoot ourselves ! at least we'll know why.

72
00:03:51,286 --> 00:03:56,213
- And you do this number to each of your clients?
- Yes - and it gets better every time! Honing my art!

73
00:03:56,247 --> 00:03:58,019
- Here.
- Thanks!

74
00:04:11,803 --> 00:04:14,515
- Hey let's hurry a bit, we need 2000 of these by 6PM okay?

75
00:04:14,540 --> 00:04:16,498
- Yeah yeah, sure.

76
00:04:20,043 --> 00:04:22,969
Hmmmmm what do I see?

77
00:04:23,019 --> 00:04:25,215
A piece of paper...

78
00:04:31,561 --> 00:04:33,883
Aaah, and something's written on it!

79
00:04:35,284 --> 00:04:37,831
Perhaps a message...

80
00:04:40,728 --> 00:04:46,971
"Make your <i>éclairs</i> yourselves"... Well here's a lesson!

81
00:04:47,012 --> 00:04:51,805
"We'd like to go to the beach" - what a cheek!

82
00:04:52,546 --> 00:04:56,015
"<i>Choux</i> pastry recipe" - now it gets interesting.

83
00:04:56,662 --> 00:04:59,709
"Take a hundred and fifty grams of flour... "

84
00:05:00,316 --> 00:05:01,996
"Three eggs..."

85
00:05:02,021 --> 00:05:03,740
"Fifty grams of butter..."

86
00:05:03,765 --> 00:05:06,289
- Sir, did you hear about "Year Zero-One"?

87
00:05:06,314 --> 00:05:09,581
- Well... young folk talk about it.

88
00:05:10,548 --> 00:05:13,480
- Do you think stopping everything would be a solution?

89
00:05:13,529 --> 00:05:15,373
- Well, eh!

90
00:05:16,061 --> 00:05:17,096
- One's gotta eat!

91
00:05:17,121 --> 00:05:18,682
- You think we'll always need fish, then?

92
00:05:18,714 --> 00:05:22,817
- Naturally we will! Although now, we catch less and less.

93
00:05:22,864 --> 00:05:26,155
The smell of gas, polluted water...

94
00:05:26,676 --> 00:05:29,134
- But, er... what if everything stops?

95
00:05:29,356 --> 00:05:33,576
- Even if everything stops, I'll do something - I can do many things.

96
00:05:33,993 --> 00:05:35,482
I won't be left out.

97
00:05:35,507 --> 00:05:39,265
- Walls aren't enough for them, they have to write "Zero-One" in the sky now!

98
00:05:39,290 --> 00:05:42,665
I think it's an ad of some sort... for a brand of toothpaste or detergent.

99
00:05:42,705 --> 00:05:46,427
- I'd be surprised if it were... we know too well what those things are, now.

100
00:05:46,452 --> 00:05:48,081
"Zero-One"... it's "Zero-One".

101
00:05:48,114 --> 00:05:49,761
- Ah, don't count me in on that!

102
00:05:49,786 --> 00:05:54,842
- Suppose everyone goes, suppose that there's a general mobilization: what will you do?

103
00:05:54,867 --> 00:05:57,957
- Ah well if it's a general mobilization then it's different... one <i>has</i> to go!

104
00:05:57,982 --> 00:05:59,895
<i>Obligated</i>, even.
- ...or happy to go!

105
00:05:59,920 --> 00:06:03,203
Because it lets you lead the life you dreamt of as a kid...

106
00:06:03,228 --> 00:06:04,850
The only problem is, at war you get killed!

107
00:06:04,875 --> 00:06:07,680
Well "Zero-One" is the war, without the war.

108
00:06:07,705 --> 00:06:10,294
And everyone will go: men, women, children.

